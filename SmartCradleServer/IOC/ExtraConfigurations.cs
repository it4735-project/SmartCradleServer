using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using MongoDB.Driver;
using MQTTnet;
using MQTTnet.Client;
using SmartCradleServer.Infrastructures;
using SmartCradleServer.Models;
using SmartCradleServer.Services;

namespace SmartCradleServer.IOC;

public static class ExtraConfigurations
{
    public static async Task Register(this IServiceCollection services, IConfiguration configuration)
    {
        await ConnectMqtt(services, configuration);
        ConfigureMongoDb(services, configuration);
        ConfigureSwagger(services, configuration);
        ConfigureAuthentication(services, configuration);

        // Register services
        services.AddSingleton<MqttService>();
        services.AddSingleton<DeviceService>();
        services.AddSingleton<UserService>();
        services.AddSingleton<TokenService>();
        services.AddSingleton<AuthService>();
    }

    private static void ConfigureMongoDb(this IServiceCollection services, IConfiguration configuration)
    {
        var mongoDbSettings = configuration.GetSection("MongoDb").Get<MongoDbSettings>();
        var mongoClient = new MongoClient(mongoDbSettings.ConnectionString);
        var database = mongoClient.GetDatabase(mongoDbSettings.DatabaseName);

        // Register deviceCollection
        var deviceCollection = database.GetCollection<Device>("devices");
        var userCollection = database.GetCollection<User>("users");
        services.AddSingleton(deviceCollection);
        services.AddSingleton(userCollection);
    }

    private static async Task ConnectMqtt(this IServiceCollection services, IConfiguration configuration)
    {
        try
        {
            var mqttClientSettings = configuration.GetSection("Mqtt").Get<MqttClientSettings>();
            var mqttClientOptions = new MqttClientOptionsBuilder()
                .WithTcpServer(mqttClientSettings.Broker, mqttClientSettings.Port).Build();

            var mqttFactory = new MqttFactory();
            var mqttClient = mqttFactory.CreateMqttClient();

            await mqttClient.ConnectAsync(mqttClientOptions, CancellationToken.None);

            // Subscribe to topics
            await mqttClient.SubscribeAsync(new MqttTopicFilterBuilder().WithTopic("it4735/smart-cradle/temperature")
                .Build());

            services.AddSingleton(mqttClient);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
    
    private static void ConfigureSwagger(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddSwaggerGen(option =>
        {
            option.SwaggerDoc("v1", new OpenApiInfo { Title = "Smart Cradle API", Version = "v1" });
            option.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                In = ParameterLocation.Header,
                Description = "Please insert JWT with Bearer into field",
                Name = "Authorization",
                Type = SecuritySchemeType.Http,
                BearerFormat = "JWT",
                Scheme = "Bearer"
            });
            option.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        }
                    },
                    new string[] { }
                }
            });
        });
    }
    
    private static void ConfigureAuthentication(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddAuthentication(x =>
        {
            x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        }).AddJwtBearer(x =>
        {
            x.RequireHttpsMetadata = false;
            x.SaveToken = true;
            x.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey =
                    new SymmetricSecurityKey(
                        Encoding.ASCII.GetBytes(configuration.GetSection("JwtKey").ToString() ?? throw new InvalidOperationException())),
                ValidateIssuer = false,
                ValidateAudience = false
            };
        });
    }
}