using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace SmartCradleServer.Models;

public class User
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string? Id { get; set; } = null;

    [BsonElement("name")] public string? Name { get; set; } = null;
    
    [BsonElement("email")]
    public string Email { get; set; }
    
    [BsonElement("password")]
    public string Password { get; set; }
    [BsonElement("role")]
    public string Role { get; set; }
}