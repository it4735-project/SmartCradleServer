using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace SmartCradleServer.Models;

public class Device
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string? Id { get; set; } = null;

    [BsonElement("name")] public string Name { get; set; } = null!;
    [BsonElement("owner_id")] public string? OwnerId { get; set; }
    [BsonElement("created_at")] public DateTime CreatedAt { get; set; }
    [BsonElement("updated_at")] public DateTime UpdatedAt { get; set; }
}