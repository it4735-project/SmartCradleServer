using System.ComponentModel.DataAnnotations;
using SmartCradleServer.Models;

namespace SmartCradleServer.DTO;

public class UserDto
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
    public string? Role { get; set; } = "user";
}

public class UserLoginDto
{
    public string Email { get; set; }
    public string Password { get; set; }
}

public class UserRegisterDto
{
    public string Name { get; set; }
    public string Email { get; set; }
    [Required(AllowEmptyStrings =false,ErrorMessage ="Please enter the password")]
    public string? Password { get; set; }
    [Compare("Password")]
    [Required]
    public string? ConfirmPassword { get; set; }
}

public class LoginResponseDto
{
    public string Token { get; set; }
    public User User { get; set; }
}