using Microsoft.AspNetCore.Identity;
using MongoDB.Driver;
using SmartCradleServer.DTO;
using SmartCradleServer.Models;

namespace SmartCradleServer.Services;

public class AuthService
{
    private readonly IMongoCollection<User> _usersCollection;
    private readonly TokenService _tokenService;
    private readonly UserService _userService;
    
    public AuthService(IMongoCollection<User> usersCollection, TokenService tokenService, UserService userService)
    {
        _usersCollection = usersCollection;
        _tokenService = tokenService;
        _userService = userService;
    }
    
    public async Task<string> Login(string mail, string password)
    {
        User user = _userService.GetUserByMail(mail).Result;
        //verify password derivation
        PasswordHasher<User> passwordHasher = new();
        if(passwordHasher.VerifyHashedPassword(user, user.Password, password) == PasswordVerificationResult.Success)
        {
            var token = _tokenService.GenerateToken(user);
            return token;
        } else
        {
            return null;
        }
    }

    public async Task<string> Register(UserRegisterDto userRegisterDto)
    {
        if (!Equals(userRegisterDto.Password, userRegisterDto.ConfirmPassword))
        {
            return null;
        }

        var user = new User();
        user.Name = userRegisterDto.Name;
        user.Email = userRegisterDto.Email;
        user.Password = userRegisterDto.Password;
        user.Role = "user";
        await _userService.CreateUser(user);
        
        return _tokenService.GenerateToken(user);
    }
}