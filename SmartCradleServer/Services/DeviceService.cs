using MongoDB.Driver;
using SmartCradleServer.Models;

namespace SmartCradleServer.Services;

public class DeviceService
{
    private readonly IMongoCollection<Device> _deviceCollection;
    private readonly MqttService _mqttService;


    public DeviceService(IMongoCollection<Device> deviceCollection, MqttService mqttService)
    {
        _deviceCollection = deviceCollection;
        _mqttService = mqttService;
    }

    public async Task<List<Device>> GetAsync()
    {
        await _mqttService.PublishAsync("it4735/smart-cradle/fan", "{\"value\": 1}");

        return await _deviceCollection.Find(_ => true).ToListAsync();
    }

    public async Task<Device?> GetAsync(string id) =>
        await _deviceCollection.Find(x => x.Id == id).FirstOrDefaultAsync();

    public async Task CreateAsync(Device newDevice) =>
        await _deviceCollection.InsertOneAsync(newDevice);

    public async Task UpdateAsync(string id, Device updatedDevice) =>
        await _deviceCollection.ReplaceOneAsync(x => x.Id == id, updatedDevice);

    public async Task RemoveAsync(string id) =>
        await _deviceCollection.DeleteOneAsync(x => x.Id == id);
}