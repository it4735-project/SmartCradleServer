using MQTTnet;
using MQTTnet.Client;

namespace SmartCradleServer.Services;

public class MqttService
{
    private readonly IMqttClient _mqttClient;

    public MqttService(IMqttClient mqttClient)
    {
        _mqttClient = mqttClient;
    }

    public async Task PublishAsync(string topic, string message)
    {
        var mqttMessage = new MqttApplicationMessageBuilder()
            .WithTopic(topic)
            .WithPayload(message)
            .WithRetainFlag()
            .Build();

        var res = await _mqttClient.PublishAsync(mqttMessage, CancellationToken.None);
    }

    public async Task SubscribeAsync(string topic)
    {
        await _mqttClient.SubscribeAsync(new MqttTopicFilterBuilder()
            .WithTopic(topic)
            .Build());
    }
}