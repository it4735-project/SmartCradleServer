using Microsoft.AspNetCore.Identity;
using MongoDB.Driver;
using SmartCradleServer.DTO;
using SmartCradleServer.Models;

namespace SmartCradleServer.Services;

public class UserService
{
    private readonly IMongoCollection<User> _usersCollection;
    private readonly TokenService _tokenService;
    
    public UserService(IMongoCollection<User> usersCollection, TokenService tokenService)
    {
        _usersCollection = usersCollection;
        _tokenService = tokenService;
    }
    
    public async Task<List<User>> GetUsers()
    {
        return await _usersCollection.Find(user => true).ToListAsync();
    }
    
    public async Task<User> GetUser(string id)
    {
        return await _usersCollection.Find(user => user.Id == id).FirstOrDefaultAsync();
    }
    
    public async Task<User> CreateUser(User user)
    {
        PasswordHasher<User> passwordHasher = new();
        string hashedPassword = passwordHasher.HashPassword(user, user.Password);
        user.Password = hashedPassword;
        await _usersCollection.InsertOneAsync(user);
        return user;
    }
    
    public async Task UpdateUser(string id, User user)
    {
        await _usersCollection.ReplaceOneAsync(u => u.Id == id, user);
    }
    
    public async Task DeleteUser(string id)
    {
        await _usersCollection.DeleteOneAsync(user => user.Id == id);
    }

    public async Task<User> GetUserByMail(string mail)
    {
        return await _usersCollection.Find(user => user.Email == mail).FirstOrDefaultAsync();
    }
    
}