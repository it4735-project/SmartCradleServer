using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartCradleServer.DTO;
using SmartCradleServer.Models;
using SmartCradleServer.Services;

namespace SmartCradleServer.Controllers;

[AllowAnonymous]
[Route("api/auth")]
public class AuthController : ControllerBase
{
    private readonly AuthService _authService;
    private readonly UserService _userService;
    public AuthController(AuthService authService, UserService userService)
    {
        this._authService = authService;
        this._userService = userService;
    }
    
    [HttpPost("login")]
    public async Task<IActionResult> Login([FromBody] UserLoginDto user)
    {
        var token = await _authService.Login(user.Email, user.Password);
        
        if (token == null)
            return new UnauthorizedResult();
        return Ok(new {token, message="Login successful"});
    }
    
    [HttpPost("register")]
    public async Task<IActionResult> Register([FromBody] UserRegisterDto user)
    {
        if (!ModelState.IsValid)
        {
            return new BadRequestObjectResult(ModelState);
        }
        //logging user to console
        Console.WriteLine(user.Password);
        User emailExist = await _userService.GetUserByMail(user.Email);
        if (emailExist != null)
        {
            return new BadRequestObjectResult(
               new {
                    message = "Email already exists",
                });
        }
        var token = await _authService.Register(user)!;
        if(token== null)
            return new BadRequestObjectResult(
               new {
                    message = "Something went wrong",
                });
        
        return Ok(new {token, message="Register complete" });
    }   
}