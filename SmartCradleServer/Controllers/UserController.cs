using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartCradleServer.Models;
using SmartCradleServer.Services;

namespace SmartCradleServer.Controllers;
[ApiController]
[Authorize]
[Route("api/User")]
public class UserController : ControllerBase
{
    private readonly UserService _userService;
    
    public UserController(UserService userService)
    {
        _userService = userService;
    }
    
    [Authorize(Roles = "admin")]
    [HttpGet("GetAllUser")]
    public async Task<ActionResult<IEnumerable<User>>> Get()
    {
        var users = await _userService.GetUsers();
        return users;
    }
    
    [HttpGet("GetUser/{id}")]
    public async Task<ActionResult<User>> Get(string id)
    {
        var user = await _userService.GetUser(id);
        return user;
    }
    
    [HttpGet("GetMe")]
    public async Task<ActionResult<User>> GetMe()
    {
        var userId = User.FindFirstValue(ClaimTypes.Name);
        var user = await _userService.GetUser(userId);
        return user;
    }

    [HttpPost("CreateUser")]
    public async Task<ActionResult<User>> Create(User user)
    {
        await _userService.CreateUser(user);
        return user;
    }
    
    [HttpPut("UpdateUser/{id}")]
    public async Task<ActionResult<User>> Update(string id, User user)
    {
        await _userService.UpdateUser(id, user);
        return user;
    }
    
    [HttpDelete("DeleteUser/{id}")]
    public async Task<ActionResult<User>> Delete(string id)
    {
        await _userService.DeleteUser(id);
        return NoContent();
    }
    
    [HttpGet("GetUserByMail/{mail}")]
    public async Task<ActionResult<User>> GetByMail(string mail)
    {
        var user = await _userService.GetUserByMail(mail);
        return user;
    }
}