using Microsoft.AspNetCore.Mvc;
using SmartCradleServer.Models;
using SmartCradleServer.Services;

namespace SmartCradleServer.Controllers;

[ApiController]
[Route("api/Devices")]
public class DeviceController : ControllerBase
{
    private readonly DeviceService _deviceService;

    public DeviceController(DeviceService deviceService)
    {
        _deviceService = deviceService;
    }

    [HttpGet("AdminGetAllDevices")]
    public async Task<IEnumerable<Device>> AdminGetAllDevices()
    {
        var devices = await _deviceService.GetAsync();

        return devices;
    }
}