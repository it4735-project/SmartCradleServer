namespace SmartCradleServer.Infrastructures;

public class MqttClientSettings
{
    public string Broker { get; set; } = null!;
    public int? Port { get; set; }
}